#!/bin/bash

# Check for upgradable packages
upgradable=$(pacman -Quq)

if [[ -n "${upgradable}" ]]; then
    # Loop through the upgradable packages
    for pkg in ${upgradable}; do
        # Get the build date of the package
        builddate=$(date -d "$(pacman -Si ${pkg} | grep "Build Date" | cut -d : -f 2-)" +%s)

        # Get the current date minus 2 days
        twodaysago=$(date -d "2 days ago" +%s)

        # Compare the build date to the current date
        if [[ "${builddate}" -lt "${twodaysago}" ]]; then
            # If the package was built more than 2 days ago, attempt to upgrade it
            pacman -S --noconfirm --ask=4 ${pkg} || {

                echo "Upgrade of ${pkg} failed, manual intervention required."
                /usr/bin/notify-send -u critical -i dialog-warning "Package Update Failed" "Upgrade of ${pkg} failed, manual intervention required."
                exit 1
            }
        fi
    done
fi

# Apply the previously downloaded AUR updates
yay -Su --noconfirm --ask=4 || {

    echo "AUR upgrade failed, manual intervention required."
    /usr/bin/notify-send -u critical -i dialog-warning "AUR Update Failed" "Upgrade failed, manual intervention required."
    exit 1
}


# Check if Flatpak is installed
if command -v flatpak &>/dev/null; then
    # Update Flatpak applications
    flatpak update -y || {

        echo "Flatpak upgrade failed, manual intervention required."
        /usr/bin/notify-send -u critical -i dialog-warning "Flatpak Update Failed" "Upgrade failed, manual intervention required."
        exit 1
    }
fi
