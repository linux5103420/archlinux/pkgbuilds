#!/bin/bash

# Check for updates
updates=$(checkupdates)

if [[ -n "${updates}" ]]; then
    # Download updates with pacman, using trickle to limit the bandwidth to 100KB/s
    trickle -s -d 1000 pacman -Syuw --noconfirm
fi

# Check for AUR updates
aur_updates=$(yay -Qum)

if [[ -n "${aur_updates}" ]]; then
    # Download AUR updates with yay, using trickle to limit the bandwidth to 100KB/s
    trickle -s -d 1000 yay -Syuw --noconfirm
fi

# If Flatpak is installed, update Flatpak applications
if command -v flatpak &>/dev/null; then
    # Update Flatpak applications, using trickle to limit the bandwidth to 100KB/s
    trickle -s -d 1000 flatpak update -y
fi
